package com.example.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class ActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivityApplication.class, args);
	}
	@GetMapping("/users")
	public String getUsers(){
		return"All users retrieved";
	}
	@PostMapping ("/users")
	public String createUsers(){
		return"new user created";
	}
	@GetMapping("/users/{id}")
	public String getUser(@PathVariable(value="id")long userid){
		return "hi user no."+userid+"";
	}
	@PutMapping("/users/{id}")
	@ResponseBody
	public User updateUser(@RequestBody User user){
		return user;
	}
	@DeleteMapping("/users/{id}")
	public String updateUser(@PathVariable(value="id")long userid,@RequestHeader(value = "Authorization")String token){
		if(token.equals("")||token.equals(null)||token.isEmpty()){
			return "Unauthorized access";
		}
		else{
			return "The user "+userid+" has been deleted";
		}
	}
}
